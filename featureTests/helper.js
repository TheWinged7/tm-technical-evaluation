var supertest = require('supertest');
const url = require('url')
const fs = require('fs');

// Set local testing variables
let useLocalService = true
let protocol = process.env.PROTOCOL || 'http'
let hostname = process.env.HOST || 'localhost'
let port = process.env.PORT || '3000'
let baseUrl = new URL(`${protocol}://${hostname}:${port}`)

// Attempt to load pipeline testing variables if they exist
const stackFile = process.env.STACK_JSON_FILE
if (fs.existsSync(stackFile)) {
    const stackInfo = JSON.parse(fs.readFileSync(stackFile))
    useLocalService = false
    endpoint = stackInfo.ServiceEndpoint
    baseUrl = new URL(`${endpoint}/`)
}

const request = supertest(baseUrl.href);

module.exports =
{
    request
}