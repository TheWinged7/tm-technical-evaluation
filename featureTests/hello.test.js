const request = require('./helper');

describe("GET response from hello", () => {
    try {
        test("GET valid response message from hello", async done => {
            let expected_statuses = [200, 400, 500]
            let expected_responses = [
                'All good! Your function executed successfully.',
                'Error: You did something wrong!',
                'Error: Oops! We did something wrong.',
              ]
            await request.request.get(`hello`)
                .then((response) => {
                    expect(expected_statuses).toContain(response.statusCode);
                    expect(response.body.message).toBeDefined();
                    expect(expected_responses).toContain(response.body.message);
                    done();
                })
        });

        test("GET query params returned in input from hello", async done => {
            let expected_statuses = [200, 400, 500]
            await request.request.get(`hello?world=hello`)
                .then((response) => {
                    expect(expected_statuses).toContain(response.statusCode);
                    expect(response.body.input.queryStringParameters).toBeDefined();
                    expect(response.body.input.queryStringParameters).toEqual({ 'world': 'hello' });
                    done();
                })
        })
    }
    catch (err) {
        console.log("EXCEPTION : ", err);
    }
})
