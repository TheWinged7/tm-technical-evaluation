'use strict';

const mod = require('./handler');

const jestPlugin = require('serverless-jest-plugin');
const lambdaWrapper = jestPlugin.lambdaWrapper;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'hello' });

describe('hello', () => {
  it('returns one of the expected responses body', () => {
    let expected_responses = [
      'All good! Your function executed successfully.',
      'Error: You did something wrong!',
      'Error: Oops! We did something wrong.',
    ]
    let expected_statuses = [200, 400, 500]
    let input = { queryStringParameters: { a: 'b' } }
    return wrapped.run(input).then((response) => {
      expect(expected_statuses).toContain(response.statusCode);
      expect(expected_responses).toContain(JSON.parse(response.body).message);
      expect(JSON.parse(response.body).input).toEqual(input);
    });
  });
  
});
